var gl;
var canvas;
var shaderProgram;
var vertexPositionBuffer;

// Create a place to store terrain geometry
var tVertexPositionBuffer;

//Create a place to store normals for shading
var tVertexNormalBuffer;

// Create a place to store the terrain triangles
var tIndexTriBuffer;

//Create a place to store the traingle edges
var tIndexEdgeBuffer;

// View parameters
var eyePt = vec3.fromValues(0.0,0.0,0.0);
var viewDir = vec3.fromValues(0.0,0.0,-1.0);
var up = vec3.fromValues(0.0,1.0,0.0);
var viewPt = vec3.fromValues(0.0,0.0,0.0);

// Create the normal
var nMatrix = mat3.create();

// Create ModelView matrix
var mvMatrix = mat4.create();

//Create Projection matrix
var pMatrix = mat4.create();

var mvMatrixStack = [];

//Held down keys for interactivity
var currentlyPressedKeys= {}

//default params
var DEFAULT_SPEED = 0.001;
var DEFAULT_ACCEL = DEFAULT_SPEED / 20;
var DEFAULT_ROT_SPEED = 0.005;
var DEFAULT_PIT_SPEED = 0.005;

//init
//speed of viewpoint
var speed = DEFAULT_SPEED;
var accel = DEFAULT_ACCEL; 

//rolling parameters + relevant quaternions
var rotationSpeed = DEFAULT_ROT_SPEED;
var rotationQuatLeft = quat.create();
var rotationQuatRight = quat.create();

//pitching parameters + relevant quaternions
var pitchSpeed = DEFAULT_PIT_SPEED;
var rotationQuatUp = quat.create();
var rotationQuatDown = quat.create();
var sidewaysVec = vec3.create();

//-------------------------------------------------------------------------
/**
 * Populates terrain buffers for terrain generation
 */
function setupTerrainBuffers() {	
	var vTerrain=[];
	var fTerrain=[];
	var nTerrain=[];
	var eTerrain=[];
	var n = 7;
	var gridN = Math.pow(2,n);
	
	var numT = terrainFromIteration(gridN, -1,1,-1,1, vTerrain, fTerrain, nTerrain);
	console.log("Generated ", numT, " triangles"); 
	tVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, tVertexPositionBuffer);      
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vTerrain), gl.STATIC_DRAW);
	tVertexPositionBuffer.itemSize = 3;
	tVertexPositionBuffer.numItems = (gridN+1)*(gridN+1);
	
	// Specify normals to be able to do lighting calculations
	tVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, tVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(nTerrain),
								gl.STATIC_DRAW);
	tVertexNormalBuffer.itemSize = 3;
	tVertexNormalBuffer.numItems = (gridN+1)*(gridN+1);
	
	// Specify faces of the terrain 
	tIndexTriBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, tIndexTriBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(fTerrain),
								gl.STATIC_DRAW);
	tIndexTriBuffer.itemSize = 1;
	tIndexTriBuffer.numItems = numT*3;
	
	//Setup Edges
	generateLinesFromIndexedTriangles(fTerrain,eTerrain);  
	tIndexEdgeBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, tIndexEdgeBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(eTerrain),
								gl.STATIC_DRAW);
	tIndexEdgeBuffer.itemSize = 1;
	tIndexEdgeBuffer.numItems = eTerrain.length;
}

//-------------------------------------------------------------------------
/**
 * Draws terrain from populated buffers
 */
function drawTerrain(){
	gl.polygonOffset(0,0);
	gl.bindBuffer(gl.ARRAY_BUFFER, tVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, tVertexPositionBuffer.itemSize, 
							gl.FLOAT, false, 0, 0);

	// Bind normal buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, tVertexNormalBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, tVertexNormalBuffer.itemSize,
							gl.FLOAT, false, 0, 0);   
		
	//Draw 
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, tIndexTriBuffer);
	gl.drawElements(gl.TRIANGLES, tIndexTriBuffer.numItems, gl.UNSIGNED_SHORT,0);      
}

//-------------------------------------------------------------------------
/**
 * Draws edge of terrain from the edge buffer
 */
function drawTerrainEdges(){
	gl.polygonOffset(1,1);
 	gl.bindBuffer(gl.ARRAY_BUFFER, tVertexPositionBuffer);
 	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, tVertexPositionBuffer.itemSize, 
							gl.FLOAT, false, 0, 0);

	// Bind normal buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, tVertexNormalBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, tVertexNormalBuffer.itemSize,
							gl.FLOAT, false, 0, 0);   
		
	//Draw 
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, tIndexEdgeBuffer);
	gl.drawElements(gl.LINES, tIndexEdgeBuffer.numItems, gl.UNSIGNED_SHORT,0);      
}

//-------------------------------------------------------------------------
/**
 * Sends Modelview matrix to shader
 */
function uploadModelViewMatrixToShader() {
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

//-------------------------------------------------------------------------
/**
 * Sends projection matrix to shader
 */
function uploadProjectionMatrixToShader() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, 
											false, pMatrix);
}

//-------------------------------------------------------------------------
/**
 * Generates and sends the normal matrix to the shader
 */
function uploadNormalMatrixToShader() {
	mat3.fromMat4(nMatrix,mvMatrix);
	mat3.transpose(nMatrix,nMatrix);
	mat3.invert(nMatrix,nMatrix);
	gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, nMatrix);
}

//----------------------------------------------------------------------------------
/**
 * Pushes matrix onto modelview matrix stack
 */
function mvPushMatrix() {
	var copy = mat4.clone(mvMatrix);
	mvMatrixStack.push(copy);
}


//----------------------------------------------------------------------------------
/**
 * Pops matrix off of modelview matrix stack
 */
function mvPopMatrix() {
	if (mvMatrixStack.length == 0) {
		throw "Invalid popMatrix!";
	}
	mvMatrix = mvMatrixStack.pop();
}

//----------------------------------------------------------------------------------
/**
 * Sends projection/modelview matrices to shader
 */
function setMatrixUniforms() {
	uploadModelViewMatrixToShader();
	uploadNormalMatrixToShader();
	uploadProjectionMatrixToShader();
}

//----------------------------------------------------------------------------------
/**
 * Translates degrees to radians
 * @param {Number} degrees Degree input to function
 * @return {Number} The radians that correspond to the degree input
 */
function degToRad(degrees) {
	return degrees * Math.PI / 180;
}

//----------------------------------------------------------------------------------
/**
 * Creates a context for WebGL
 * @param {element} canvas WebGL canvas
 * @return {Object} WebGL context
 */
function createGLContext(canvas) {
	var names = ["webgl", "experimental-webgl"];
	var context = null;
	for (var i=0; i < names.length; i++) {
		try {
			context = canvas.getContext(names[i]);
		} catch(e) {}
		if (context) {
			break;
		}
	}
	if (context) {
		context.viewportWidth = canvas.width;
		context.viewportHeight = canvas.height;
	} else {
		alert("Failed to create WebGL context!");
	}
	return context;
}

//----------------------------------------------------------------------------------
/**
 * Loads Shaders
 * @param {string} id ID string for shader to load. Either vertex shader/fragment shader
 */
function loadShaderFromDOM(id) {
	var shaderScript = document.getElementById(id);
	
	// If we don't find an element with the specified id
	// we do an early exit 
	if (!shaderScript) {
		return null;
	}
	
	// Loop through the children for the found DOM element and
	// build up the shader source code as a string
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
		if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}
 
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}
 
	gl.shaderSource(shader, shaderSource);
	gl.compileShader(shader);
 
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	} 
	return shader;
}

//----------------------------------------------------------------------------------
/**
 * Setup the fragment and vertex shaders
 */
function setupShaders() {
	vertexShader = loadShaderFromDOM("blinnphongshader-vs");
	fragmentShader = loadShaderFromDOM("blinnphongshader-fs");
	
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Failed to setup shaders");
	}

	gl.useProgram(shaderProgram);

	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
	gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
	shaderProgram.uniformLightPositionLoc = gl.getUniformLocation(shaderProgram, "uLightPosition");    
	shaderProgram.uniformAmbientLightColorLoc = gl.getUniformLocation(shaderProgram, "uAmbientLightColor");  
	shaderProgram.uniformDiffuseLightColorLoc = gl.getUniformLocation(shaderProgram, "uDiffuseLightColor");
	shaderProgram.uniformSpecularLightColorLoc = gl.getUniformLocation(shaderProgram, "uSpecularLightColor");
	shaderProgram.uniformDiffuseMaterialColor = gl.getUniformLocation(shaderProgram, "uDiffuseMaterialColor");
	shaderProgram.uniformAmbientMaterialColor = gl.getUniformLocation(shaderProgram, "uAmbientMaterialColor");
	shaderProgram.uniformSpecularMaterialColor = gl.getUniformLocation(shaderProgram, "uSpecularMaterialColor");

  shaderProgram.uniformShininess = gl.getUniformLocation(shaderProgram, "uShininess");    
}

//-------------------------------------------------------------------------
/**
 * Sends material information to the shader
 * @param {Float32Array} a diffuse material color
 * @param {Float32Array} a ambient material color
 * @param {Float32Array} a specular material color 
 * @param {Float32} the shininess exponent for Phong illumination
 */
function uploadMaterialToShader(dcolor, acolor, scolor,shiny) {
  gl.uniform3fv(shaderProgram.uniformDiffuseMaterialColor, dcolor);
  gl.uniform3fv(shaderProgram.uniformAmbientMaterialColor, acolor);
  gl.uniform3fv(shaderProgram.uniformSpecularMaterialColor, scolor);
    
  gl.uniform1f(shaderProgram.uniformShininess, shiny);
}

//-------------------------------------------------------------------------
/**
 * Sends light information to the shader
 * @param {Float32Array} loc Location of light source
 * @param {Float32Array} a Ambient light strength
 * @param {Float32Array} d Diffuse light strength
 * @param {Float32Array} s Specular light strength
 */
function uploadLightsToShader(loc,a,d,s) {
	gl.uniform3fv(shaderProgram.uniformLightPositionLoc, loc);
	gl.uniform3fv(shaderProgram.uniformAmbientLightColorLoc, a);
	gl.uniform3fv(shaderProgram.uniformDiffuseLightColorLoc, d);
	gl.uniform3fv(shaderProgram.uniformSpecularLightColorLoc, s);
}

//----------------------------------------------------------------------------------
/**
 * Populate buffers with data
 */
function setupBuffers() {
	setupTerrainBuffers();
}

//----------------------------------------------------------------------------------
/**
 * Draw call that applies matrix transformations to model and draws model in frame
 */
function draw() { 
	var transformVec = vec3.create();

	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// We'll use perspective 
	mat4.perspective(pMatrix,degToRad(45), gl.viewportWidth / gl.viewportHeight, 0.1, 200.0);

	// We want to look down -z, so create a lookat point in that direction    
	vec3.add(viewPt, eyePt, viewDir);
	// Then generate the lookat matrix and initialize the MV matrix to that view
	mat4.lookAt(mvMatrix,eyePt,viewPt,up);    

	//Draw Terrain
	mvPushMatrix();
	vec3.set(transformVec,0.0,-0.25,-3.0);
	mat4.translate(mvMatrix, mvMatrix,transformVec);
	mat4.rotateX(mvMatrix, mvMatrix, degToRad(-75));
	mat4.rotateZ(mvMatrix, mvMatrix, degToRad(25));     
	setMatrixUniforms();


    R = 255.0/255.0;
    G = 255.0/255.0;
    B = 255.0/255.0;

	shiny = 88;
	
	uploadLightsToShader([0.3,1,1],[0.05,0.05,0.05],[0.5,0.5,0.6],[0.1,0.9,0.9]);
	uploadMaterialToShader([R,G,B],[R,G,B],[1.0,1.0,1.0],shiny);
	drawTerrain();

	mvPopMatrix();
}

//----------------------------------------------------------------------------------
/**
 * Animation to be called from tick. Updates globals and performs animation for each tick.
 */
function animate() {
	//move eye forward
	temp = vec3.create();
	vec3.scale(temp, viewDir, speed);
	vec3.add(eyePt, eyePt, temp);
}

//manipulate viewpoint by using quarterinions (once every frame), and manipulated using keys
function handleKeys(){
	// left key
	if (currentlyPressedKeys[37]){
		//reset quat and apply to up vector
		quat.setAxisAngle(rotationQuatLeft, viewDir, -rotationSpeed);
		vec3.transformQuat(up, up, rotationQuatLeft);
	}
	// right key
	if (currentlyPressedKeys[39]){
		//reset quat and apply to up vector
		quat.setAxisAngle(rotationQuatRight, viewDir, rotationSpeed);
		vec3.transformQuat(up, up, rotationQuatRight);
	}
	// up key
	if (currentlyPressedKeys[38]){
		//generate third orthogonal vector (sideways)
		//reset quat to rotate relative to sideways vector
		//apply to up and viewDir
		vec3.cross(sidewaysVec, up, viewDir);
		quat.setAxisAngle(rotationQuatUp, sidewaysVec, -pitchSpeed);
		vec3.transformQuat(up, up, rotationQuatUp);
		vec3.transformQuat(viewDir, viewDir, rotationQuatUp);
	}
	// down key
	if (currentlyPressedKeys[40]){
		//generate third orthogonal vector (sideways)
		//reset quat to rotate relative to sideways vector
		//apply to up and viewDir
		vec3.cross(sidewaysVec, up, viewDir);
		quat.setAxisAngle(rotationQuatDown, sidewaysVec, pitchSpeed);
		vec3.transformQuat(up, up, rotationQuatDown);
		vec3.transformQuat(viewDir, viewDir, rotationQuatDown);
	}
	// + or = key
	if (currentlyPressedKeys[187]){
		speed += accel
	}
	// - key
	if (currentlyPressedKeys[189]){
		if (speed - accel > 0){
			speed -= accel
		}	
	}
	// r key
	if (currentlyPressedKeys[82]){
		reset();
	}
}
 
//set inputted key to be considered pressed down
function handleKeyDown(event) {
	currentlyPressedKeys[event.keyCode] = true;
}

//set inputted key to be considered not pressed down
function handleKeyUp(event) {
	currentlyPressedKeys[event.keyCode] = false;
}

//start from beginning
function reset() {
	//reset vectors
	eyePt = vec3.fromValues(0.0,0.0,0.0);
	viewDir = vec3.fromValues(0.0,0.0,-1.0);
	up = vec3.fromValues(0.0,1.0,0.0);
	viewPt = vec3.fromValues(0.0,0.0,0.0);
	
	//reset params
	speed = DEFAULT_SPEED;
	accel = DEFAULT_ACCEL;
	rotationSpeed = DEFAULT_ROT_SPEED;
	pitchSpeed = DEFAULT_PIT_SPEED;
}

//----------------------------------------------------------------------------------
/**
 * Startup function called from html code to start program.
 */
 function startup() {
	canvas = document.getElementById("myGLCanvas");
	document.onkeydown = handleKeyDown;
	document.onkeyup= handleKeyUp;
	gl = createGLContext(canvas);
	setupShaders();
	setupBuffers();
	gl.clearColor(1.0, 1.0, 1.0, 1.0);
	gl.enable(gl.DEPTH_TEST);
	// gl.enable(gl.FOG);
	// gl.Fogi(gl.FOG_MODE, gl.LINEAR);
	tick();
}

//----------------------------------------------------------------------------------
/**
 * Tick called for every animation frame.
 */
function tick() {
	requestAnimFrame(tick);
	draw();
	handleKeys();
	animate();
}

