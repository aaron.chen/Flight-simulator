# Flight-Simulator

(Implementation of MP2 in CS418 at UIUC)

Simple Flight simulator with random diamond terrain generation using WebGL.

To run, simply run mp2.html in a web browser. Controls are shown in the browser.

![Flight_animation](/sample/Flight-animation.gif)
